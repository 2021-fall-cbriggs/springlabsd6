<%@ include file="/WEB-INF/layouts/include.jsp"%>

<div>
	<h1>Car Parts &ndash; 1994 Pontiac Firebird</h1>
	<div class="row">
		<div class="col-sm-8">
			<div class="text-danger">${error}</div>
			<strong>Part Number:</strong> ${carpart.partNumber}<br /> <strong>Line:</strong>
			${carpart.line}<br /> <strong>Description:</strong>
			${carpart.description}<br />
			<br />
			<hr />
			<h2>Details</h2>
			<c:if test="${empty carpart.detailsMap}">
				<em>Sorry, no details available</em>
			</c:if>
			<c:forEach items="${carpart.detailsMap}" var="entry">
				<strong><c:out value="${entry.key}" /></strong> : 
                        <c:out value="${entry.value}" />
				<br />
			</c:forEach>
			<hr />
			<h2>Store Locations</h2>
			<div class="">
			<c:choose>
				<c:when test="${not empty carpart.storeLocations}">
					<c:forEach items="${carpart.storeLocations}" var="element">
				        <%-- 
								OPTION 1
						 --%>
						<c:out value="${element}" /><br/>

						<%-- 
								OPTION 2
								Remember to switch the toString() return statement 
								in StoreLocation.java
						 --%>
<%-- 						<c:out value="${element}" escapeXml="false" /> --%>
					
						<%-- 
								OPTION 3
						 --%>
<%-- 						 ${element.storeNumber} - ${element.storeDetails}<br /> --%>
					</c:forEach>
					
					
					
					<%-- 
							OPTION 4							
					--%>
<%-- 					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th>Store #</th><th>Store Address</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${carpart.storeLocations}" var="element">
								<tr>
									<td>${element.storeNumber}</td>
									<td>${element.storeDetails}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table> --%>
					
					
				</c:when>
				<c:otherwise>
					<em>Sorry, not available in stores</em>
				</c:otherwise>
			</c:choose>
			</div>
		</div>
		<div class="col-sm-4">
			<c:if test="${not empty carpart.imageName}">
				<img class="img-fluid"
					src="<c:url value='/resources/img/carparts/${carpart.imageName}'/>" />
			</c:if>
		</div>
	</div>
</div>