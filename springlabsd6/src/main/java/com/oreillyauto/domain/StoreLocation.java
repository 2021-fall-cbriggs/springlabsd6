package com.oreillyauto.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@Immutable
@Table(name = "STORELOCATIONS")
@Entity
public class StoreLocation implements Serializable {
    private static final long serialVersionUID = -2911787569834173812L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "store_id", columnDefinition = "INTEGER")
    private Integer storeId;
    
    @Column(name = "STORE_NUMBER", columnDefinition = "INTEGER")
    private Integer storeNumber;

    @Column(name = "STORE_DETAILS", columnDefinition = "VARCHAR(256)")
    private String storeDetails;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PART_NUMBER", referencedColumnName = "PART_NUMBER", columnDefinition = "VARCHAR(64)")
    private Carpart carpart;

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(Integer storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getStoreDetails() {
        return storeDetails;
    }

    public void setStoreDetails(String storeDetails) {
        this.storeDetails = storeDetails;
    }

    public Carpart getCarpart() {
        return carpart;
    }

    public void setCarpart(Carpart carpart) {
        this.carpart = carpart;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((carpart == null) ? 0 : carpart.hashCode());
        result = prime * result + ((storeDetails == null) ? 0 : storeDetails.hashCode());
        result = prime * result + ((storeId == null) ? 0 : storeId.hashCode());
        result = prime * result + ((storeNumber == null) ? 0 : storeNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StoreLocation other = (StoreLocation) obj;
        if (carpart == null) {
            if (other.carpart != null)
                return false;
        } else if (!carpart.equals(other.carpart))
            return false;
        if (storeDetails == null) {
            if (other.storeDetails != null)
                return false;
        } else if (!storeDetails.equals(other.storeDetails))
            return false;
        if (storeId == null) {
            if (other.storeId != null)
                return false;
        } else if (!storeId.equals(other.storeId))
            return false;
        if (storeNumber == null) {
            if (other.storeNumber != null)
                return false;
        } else if (!storeNumber.equals(other.storeNumber))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "StoreLocation [storeId=" + storeId + ", storeNumber=" + storeNumber + 
        		", storeDetails=" + storeDetails + "]";
    	
//    	return "StoreLocation [storeId=" + storeId + ", storeNumber=" + storeNumber + 
//    			", storeDetails=" + storeDetails + "]<br/>";
    }
    
}
