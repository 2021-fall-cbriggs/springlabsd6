package com.oreillyauto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.Carpart;
import com.oreillyauto.service.CarpartsService;

@Service 
public class CarpartServiceImpl implements CarpartsService {
	@Autowired
	private CarpartsRepository carpartsRepo;

    @Override
    public Carpart getCarpartByPartNumber(String partNumber) {
    	// SPRING DATA API
//    	return carpartsRepo.findByPartNumber(partNumber);
    	
    	// CRUD LIBRARY/API
    	return carpartsRepo.findById(partNumber).get(); // cheat!
    	
    	// VIA QUERY DSL
//    	return carpartsRepo.getCarpart(partNumber);
    }
    
    @Override
    public Carpart getCarpart(String partNumber) throws Exception {
        return carpartsRepo.getCarpart(partNumber);
    }

	@Override
	public boolean saveCarpart(Carpart carpart) {
		Carpart savedCarpart = carpartsRepo.save(carpart);
		return savedCarpart != null;
	}

	@Override
	public void deleteCarpartByPartNumber(Carpart carpart) {
	    carpartsRepo.delete(carpart);
	}

}

