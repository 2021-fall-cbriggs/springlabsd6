package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.oreillyauto.domain.Example;
import com.oreillyauto.service.ExampleService;

@Controller
public class ExampleController {

	@Autowired
	ExampleService exampleService;
	
	@GetMapping(value = { "/example" })
	public String login(Model model) throws Exception {
		List<Example> exampleList = exampleService.getExamples();
		ObjectMapper mapper = new ObjectMapper();
		String result = mapper.writeValueAsString(exampleList);
		model.addAttribute("result", result);
		
		String foo = new Gson().toJson(exampleList);
		
		return "example";
	}

	@GetMapping(value = { "/example/test/{day}" }) // 3 (wed) or 4 (thurs)
	public String getTest(@PathVariable String day, Model model) throws Exception {
		exampleService.testQueries(day);
		return "example";
	}

    
}
