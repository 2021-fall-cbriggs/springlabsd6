package com.oreillyauto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.oreillyauto.domain.Carpart;
import com.oreillyauto.service.CarpartsService;

@Controller
public class CarpartController {
	
	@Autowired
	private CarpartsService carpartsService;
	
	@GetMapping(value = { "/carparts" })
	public String getIndex(Model model, String firstName, String lastName) {
		String fullName = ((firstName != null && firstName.length() > 0)? firstName : "");
		fullName = ((fullName.length() > 0)? (firstName + " " + lastName) : "");
		model.addAttribute("fullName", fullName);
		return "carparts";
	}
	
	@GetMapping(value = { "/carparts/other/{partNumber}",
			              "/carparts/engine/{partNumber}",
			              "/carparts/electrical/{partNumber}"})
	public String getCarpart(@PathVariable String partNumber, Model model) {
        Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);
        model.addAttribute("carpart", carpart);
        model.addAttribute("active", partNumber);
		return "carparts";
	}

	@GetMapping(value = { "/carparts/partnumber" })
	public String getPartnumber(Model model) {
	    model.addAttribute("active", "add");
	    return "partnumber";
	}
	
	@PostMapping(value = { "/carparts/partnumber" })
	public String postPartnumber(Model model, String partnumber, String title,
			String update) {
		
		System.out.println("partnumber=>" + partnumber + " title=>" + title);
		
		// Version 1 =================================================================================
//	    return "redirect:/carparts/partnumber";
	    
	    // Version 2 =================================================================================
//	    Carpart carpart = new Carpart();
//	    carpart.setPartNumber(partnumber);
//	    carpart.setTitle(title);
//	    carpart.setLine("");
//	    carpart.setDescription("");        
//	    boolean saved = carpartsService.saveCarpart(carpart);
//	    
//	    if (saved) {
//	    	model.addAttribute("messageType", "success");
//	        model.addAttribute("message", "Part Number " + partnumber+ " Saved Successfully!");	
//	    } else {
//	    	model.addAttribute("messageType", "danger");
//	        model.addAttribute("message", "Part Number " + partnumber+ " Not Saved Successfully!");	        	
//	    }
//	    
//	    return "partnumber";
		
		// Version 3 =================================================================================
//	    Carpart carpart = new Carpart();
//	    carpart.setPartNumber(partnumber);
//	    carpart.setTitle(title);
//	    carpart.setLine("");
//	    carpart.setDescription("");        
//	    boolean saved = carpartsService.saveCarpart(carpart);
//	    
//	    if (saved) {
//	    	model.addAttribute("carpart", carpart);
//	    	model.addAttribute("messageType", "success");
//	        model.addAttribute("message", "Part Number " + partnumber+ " Saved Successfully!");	
//	    } else {
//	    	model.addAttribute("messageType", "danger");
//	        model.addAttribute("message", "Part Number " + partnumber+ " Not Saved Successfully!");	        	
//	    }
//	    
//	    return "partnumber";

	    // Version 4 =================================================================================
	    Carpart carpart = new Carpart();
	    carpart.setPartNumber(partnumber);
	    carpart.setTitle(title);
	    carpart.setLine("");
	    carpart.setDescription("");        
	    boolean saved = carpartsService.saveCarpart(carpart);
	    
	    String action = ("true".equalsIgnoreCase(update)) ? "Updated" : "Saved";
	    
        if (saved) {
            model.addAttribute("messageType", "success");
            model.addAttribute("message", "Part Number " + partnumber + " "+action+" Successfully");	
            model.addAttribute("carpart", carpart);
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Part Number " + partnumber + " Not " + action + " Successfully!");	
        }
	    
	    return "partnumber";
	}

	@GetMapping(value = { "/carparts/delete/{partnumber}" })
	public String deleteCarpart(@PathVariable String partnumber, Model model) {
	        
	    if (partnumber != null && partnumber.length() > 0) {
	        Carpart carpart = carpartsService.getCarpartByPartNumber(partnumber);
	        carpartsService.deleteCarpartByPartNumber(carpart);
	        model.addAttribute("message", "Car Part with part # = " + partnumber + " Deleted Successfully");
	    }
	    
	    return "partnumber";
	}

	
}
