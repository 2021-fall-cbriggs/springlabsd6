package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Carpart;

public interface CarpartsRepositoryCustom {
	// Create Custom Abstract Methods in the Custom Repository
    public Carpart getCarpartByPartNumber(String partNumber);
    public Carpart getCarpart(String partNumber);
    public List<Carpart> getCarparts();
	
}

